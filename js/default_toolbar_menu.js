(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.defaultToolbarMenu = {
    attach: function (context, settings) {
      console.log(settings)
      setTimeout(function () {
        if (drupalSettings.default_toolbar_menu.menu && parseInt(drupalSettings.default_toolbar_menu.user) == parseInt(settings.user.uid)) {
          $('#toolbar-item-administration').css('display', 'none');
          $('#toolbar-item-shortcuts').css('display', 'none');
          if (window.localStorage.getItem('Drupal.toolbar.activeTabID') !== '"toolbar-item-toolbar-menu-' + drupalSettings.default_toolbar_menu.menu + '"') {
            $('#toolbar-item-toolbar-menu-' + drupalSettings.default_toolbar_menu.menu).click();
          }
        }
      }, 50);
    }
  };
})(jQuery, Drupal, drupalSettings);
