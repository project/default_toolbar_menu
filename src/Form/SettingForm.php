<?php

namespace Drupal\default_toolbar_menu\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Settigns form for this module.
 */
class SettingForm extends ConfigFormBase {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new SettingForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  final public function __construct(
    EntityTypeManagerInterface $entity_type_manager
  ) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'default_toolbar_menu_setting_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Get all roles.
    /** @var \Drupal\user\RoleInterface[] $roles */
    $roles = $this->entityTypeManager->getStorage('user_role')->loadMultiple();

    $toolbar_menus = $this->entityTypeManager->getStorage('toolbar_menu_element')->loadMultiple();
    $menu_options = array_map(function ($entity) {
      /** @var \Drupal\Core\Entity\EntityInterface $entity */
      return [$entity->id() => $entity->label()];
    }, $toolbar_menus);

    $values = $this->config('default_toolbar_menu.setting')->get('default_menu');

    // Init a table to show the roles.
    $form['table'] = [
      '#type' => 'table',
      '#caption' => $this->t('Setting default toolbar menu per role'),
      '#header' => [
        $this->t('Role'),
        $this->t('Default toolbar menu'),
      ],
    ];

    foreach ($roles as $role) {
      if ($role->hasPermission('access toolbar') && !$role->isAdmin()) {
        $menu = NULL;
        foreach ($values as $value) {
          if ($value['role'] === $role->id() && !empty($value['menu'])) {
            $menu = $value['menu'];
          }
        }
        $form['table'][$role->id()] = [
          'label' => [
            '#type' => 'item',
            '#markup' => $role->label(),
          ],
          'value' => [
            '#type' => 'select',
            '#options' => $menu_options,
            '#default_value' => $menu,
          ],
        ];
      }
    }

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    $form['#tree'] = TRUE;
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Display result.
    $v = $form_state->getValues();

    $data = [];
    foreach ($v['table'] as $role => $item) {
      $data[] = [
        'role' => $role,
        'menu' => $item['value'],
      ];
    }

    $this->config('default_toolbar_menu.setting')->set('default_menu', $data)->save();
  }

  /**
   * Gets the configuration names that will be editable.
   *
   * @return array
   *   An array of configuration object names that are editable if called in
   *   conjunction with the trait's config() method.
   */
  protected function getEditableConfigNames() {
    return ['default_toolbar_menu.setting'];
  }

}
