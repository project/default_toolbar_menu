CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Provides ability to set default toolbar menu per user role.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/default_toolbar_menu

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/default_toolbar_menu


REQUIREMENTS
------------

This module requires following module outside of Drupal core:

 * Toolbar Menu - https://www.drupal.org/project/toolbar_menu


INSTALLATION
------------

 * Install the Default Toolbar Menu module as you would normally install a
   contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

 * Add some role, Add some toolbar menu, and make sure the role has the
   permission to access the toolbar and the custom toolbar menu.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Manage > Configration > User Interface > Default Toolbar Menu
    3. Select the custom toolbar menu for every role, then save.
    4. Add user with some role, logout and login with the new user with some
       role.
    5. You will see the toolbar menu switch to the setting one.


MAINTAINERS
-----------

